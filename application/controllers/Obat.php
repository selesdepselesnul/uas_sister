<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

		public function __construct()
		{
				parent::__construct();
				$this->load->model('Obat_model');
		}

		public function index()
		{
				$this->load->view('index.php');
		}

		public function getAll()
		{
				$all_obat = $this->Obat_model->getAll();

				$result = [
					"data" => $all_obat
				];

				echo json_encode((object)$result);
		}


		public function delete()
		{
				$this->Obat_model->delete($this->input->post('no_reg'));
				echo 'Data berhasil dihapus';
		}

		public function store()
		{
				$no_reg = $this->input->post('no_reg');

				$data = [
					'no_reg' => $no_reg,
					'nama' => $this->input->post('nama'),
					'jenis_obat' => $this->input->post('jenis_obat'),
					'produsen' => $this->input->post('produsen'),
					'jumlah' => $this->input->post('jumlah')
				];

				if($this->Obat_model->isExists($no_reg)) {
					$this->Obat_model->update($data);
					echo 'berhasil memperbaharui data';
				} else {
					$this->Obat_model->add($data);
					echo 'berhasil menambahkan data';
				}

		}

}
