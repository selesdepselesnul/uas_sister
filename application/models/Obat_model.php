<?php
class Obat_model extends CI_Model {

        public function __construct()
        {
            $this->load->database();
        }

        public function getAll()
        {
            $query = $this->db->get('obat');
            return $query->result_array();
        }

        public function delete($no_reg)
        {
            return $this->db->delete('obat', [
              'no_reg' => $no_reg
            ]);
        }

        public function isExists($no_reg)
        {
            return count($this->db
                        ->get_where(
                              'Obat',
                              ['no_reg' => $no_reg])
                        ->result_array()) == 1;
        }

        public function add($data)
        {
            $this->db->insert('Obat', $data);
        }

        public function update($data)
        {
            $this->db->where('no_reg', $data['no_reg']);
            $this->db->update('Obat', $data);
        }

}
