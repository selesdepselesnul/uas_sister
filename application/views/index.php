<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UAS Sister | Moch Deden | 41155050140062</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/toastr.min.css"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <h1 class="text-center">Inventory Obat</h1>
      <div class="row" style="margin-bottom: 1%; margin-top: 1%">
        <button type="button" id="addObat" class="btn btn-default">
          <span class="glyphicon glyphicon-plus"></span>Tambah
        </button>
      </div>
      <div class="row">
        <table id="obat" class="display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No. Reg</th>
              <th>Nama</th>
              <th>Jenis Obat</th>
              <th>Produsen</th>
              <th>Jumlah</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
        </table>
      </div>

      <div class="modal fade" id="obatModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Obat</h4>
            </div>
            <div class="modal-body">
              <div class="form-horizontal">
                <div class="form-group">
                 <label for="no_reg" class="col-sm-2 control-label">No. Reg</label>
                 <div class="col-sm-10">
                   <input type="text" class="form-control" id="no_reg">
                 </div>
               </div>
               <div class="form-group">
                 <label for="nama" class="col-sm-2 control-label">Nama</label>
                 <div class="col-sm-10">
                   <input type="text" class="form-control" id="nama">
                 </div>
               </div>
               <div class="form-group">
                  <label for="jenis_obat" class="col-sm-2 control-label">Jenis Obat</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="jenis_obat">
                  </div>
               </div>
               <div class="form-group">
                  <label for="produsen" class="col-sm-2 control-label">Produsen</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="produsen">
                  </div>
               </div>
               <div class="form-group">
                  <label for="jumlah" class="col-sm-2 control-label">Jumlah</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="jumlah">
                  </div>
               </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="storeObat">Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="datatables.min.js"></script>
    <script type="text/javascript" src="js/bootbox.min.js"></script>
    <script type="text/javascript" src="js/toastr.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
  </body>
</html>
