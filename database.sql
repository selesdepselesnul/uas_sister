create database uas_sister;
use uas_sister;

CREATE TABLE `obat` (
  `no_reg` varchar(15) PRIMARY KEY,
  `nama` text,
  `jenis_obat` text,
  `jumlah` bigint(20) DEFAULT NULL,
  `produsen` text
);

INSERT INTO `obat` (`no_reg`, `nama`, `jenis_obat`, `jumlah`, `produsen`) VALUES
('12', 'nama ku iki cuk cok ko iki', 'jenis obat', 22, 'produsen'),
('dddd', 'gdgdsg', 'gdgdsg', 40, 'gsdgsd');
