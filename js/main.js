$(document).ready(function() {
    var obatTable = $('#obat').DataTable({
        "ajax": "index.php/obat/getAll",
        "columns": [
            { "data": "no_reg" },
            { "data": "nama" },
            { "data": "jenis_obat" },
            { "data": "produsen" },
            { "data": "jumlah" },
            {
              "targets": -2,
              "data": null,
              "defaultContent": '<button type="button" class="btn btn-warning btn-sm edit"><span class="glyphicon glyphicon-edit"></span>Perbaharui</button>'
            },
            {
              "targets": -1,
              "data": null,
              "defaultContent": '<button type="button" class="btn btn-danger btn-sm delete"><span class="glyphicon glyphicon-remove"></span>Hapus</button>'
            }
        ]
    });

    $('#obat tbody').on( 'click', 'button', function (e) {
       var data = obatTable.row( $(this).parents('tr') ).data();
       var isDeleteAction = $(e.currentTarget).hasClass('delete');

       if(isDeleteAction) {
         bootbox.confirm({
          size: "small",
          message: "Anda yakin ingin menghapus",
          callback: function(result) {
            if(result) {
              $.post(
                'index.php/obat/delete',
                {
                    no_reg: data.no_reg
                })
                .done(function(result) {
                  toastr.success(result);
                  obatTable.ajax.reload();
                });
            }
          }
        });
       } else {
           $('#no_reg').prop('readonly', true);
           $('#no_reg').val(data.no_reg);
           $('#nama').val(data.nama);
           $('#jenis_obat').val(data.jenis_obat);
           $('#produsen').val(data.produsen);
           $('#jumlah').val(data.jumlah);
           $('#obatModal').modal('show');
       }

    });

    $('#addObat').click(function() {
        $('#no_reg').prop('readonly', false);

        $('#no_reg').val('');
        $('#nama').val('');
        $('#jenis_obat').val('');
        $('#produsen').val('');
        $('#jumlah').val('');

        $('#obatModal').modal('show');
    });


    $('#storeObat').click(function(e) {
      $.post(
        'index.php/obat/store',
        {
            no_reg: $('#no_reg').val(),
            nama: $('#nama').val(),
            jenis_obat: $('#jenis_obat').val(),
            produsen: $('#produsen').val(),
            jumlah: $('#jumlah').val()
        })
        .done(function(result) {
          toastr.success(result);
          obatTable.ajax.reload();
          $('#obatModal').modal('hide');
        });
    });

});
